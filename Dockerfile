FROM nginx

COPY index.html /usr/share/nginx/html/index.html
COPY tachyons.css /usr/share/nginx/html/tachyons.css
COPY smartos.ipxe /usr/share/nginx/html/smartos.ipxe
